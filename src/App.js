import "./App.css";

function App() {
  const name = "Ashok";
  return (
    <div>
      <h1>{name}</h1>
    </div>
  );
}

export default App;
