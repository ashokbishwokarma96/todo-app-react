import React, { useState } from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import axios from "axios";

const Todo = () => {
  const validationSchema = yup.object().shape({
    id: yup.number().required("Id is required"),
    title: yup.string().required("Title is required"),
    description: yup.string().required("Description is required"),
  });

  const columns = ["Title", "Description", "Action"];

  const [buttionStatus, setButtonStatus] = React.useState({
    save: true,
    update: false,
    reset: false,
  });

  const [initialValue, setFormValue] = useState({
    id: "",
    title: "",
    description: "",
  });

  const [data, setData] = React.useState([]);

  const onSubmit = (values) => {
    setButtonStatus({ save: true, update: false, reset: false });

    if (buttionStatus.save) {
      axios.post("http://localhost:3004/todo", values);
      setData([...data, values]);
    } else {
      setData([
        ...data.filter((el) => {
          return el.id !== values.id;
        }),
        values,
      ]);
    }

    reset();
  };

  const actionEdit = (el) => {
    setButtonStatus({ save: false, update: true, reset: true });
    todoForm.setFieldValue("id", el.id);
    todoForm.setFieldValue("title", el.title);
    todoForm.setFieldValue("description", el.description);
  };

  const actionDelete = (id) => {
    setButtonStatus({ save: true, update: false, reset: false });

    // setData((prev) => prev.filter((el) => el.id != id));
    setData(data.filter((el) => el.id !== id));
    reset();
  };

  const reset = () => {
    todoForm.resetForm();
  };

  const todoForm = useFormik({
    initialValues: initialValue,
    onSubmit,
    validationSchema,
  });

  return (
    <div style={{ margin: "10px" }}>
      <form onSubmit={todoForm.handleSubmit}>
        <div>
          <h2>TODO Form</h2>
        </div>
        <div>
          <label>Id</label>
          <br />
          <input
            type="number"
            id="id"
            onChange={todoForm.handleChange}
            value={todoForm?.values.id}
          />
          <br />
          {todoForm?.touched?.id && todoForm?.errors?.id}
        </div>
        <div>
          <label>Title</label>
          <br />
          <input
            type="text"
            id="title"
            onChange={todoForm.handleChange}
            value={todoForm?.values.title}
          />
          <br />
          {todoForm?.touched?.title && todoForm?.errors?.title}
        </div>
        <div>
          <label>Description</label>
          <br />
          <input
            type="text"
            id="description"
            onChange={todoForm.handleChange}
            value={todoForm?.values.description}
          />
          <br />
          {todoForm?.touched?.description && todoForm?.errors?.description}
        </div>
        {buttionStatus.save && (
          <button style={{ margin: "5px" }} type="submit">
            Add
          </button>
        )}
        {buttionStatus.update && (
          <button type="submit" style={{ margin: "5px" }}>
            Update
          </button>
        )}
        {buttionStatus.reset && (
          <button
            type="button"
            style={{ margin: "5px" }}
            onClick={() => reset()}
          >
            Reset
          </button>
        )}
      </form>
      <div>
        <div>
          <h2>Todo list</h2>
        </div>
        <table>
          <thead>
            <tr>
              {columns.map((el, i) => {
                return <th key={i}>{el}</th>;
              })}
            </tr>
          </thead>
          <tbody>
            {data.map((el, i) => {
              return (
                <tr key={i}>
                  <th>{el.title}</th>
                  <th>{el.description}</th>
                  <th>
                    <button
                      style={{ margin: "5px" }}
                      onClick={() => {
                        actionEdit(el);
                      }}
                    >
                      Edit
                    </button>
                    <button
                      onClick={() => {
                        actionDelete(el.id);
                      }}
                    >
                      Delete
                    </button>
                  </th>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};
export default Todo;
