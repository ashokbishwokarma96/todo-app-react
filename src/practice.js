import React from "react";

const Example = () => {
  const [userDetails, setUserDetails] = React.useState({
    id: "",
    name: "Ashok",
    department: "",
  });

  const changeName = () => {
    setUserDetails({ name: "hero" });
  };

  return (
    <div style={{ margin: "5px" }}>
      <h2>{userDetails.name}</h2>
      <button onClick={changeName}>Change Name</button>
    </div>
  );
};

export default Example;
